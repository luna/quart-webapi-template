# COPYRIGHT_HEADER


class APIError(Exception):
    status_code = 500

    def get_payload(self):
        return {}
