# COPYRIGHT_HEADER

from setuptools import setup

setup(
    name="PROJECT_NAME",
    version="0.1.0",
    description="PROJECT_DESCRIPTION",
    url="PROJECT_URL",
    author="PROJECT_AUTHOR",
    python_requires=">=3.9",
    install_requires=[
        "Quart==0.14.1",
        "quart-schema==0.7.0",
        "tomlkit==0.7.0",
        # --------------------------------------------------------------------
        # when changing these dependencies, make sure to change them in
        # .gitlab-ci.yml, too.
        "asyncpg==0.22.0",
        # ---------------------------------------------------------------------
        # migrations!
        "agnostic==1.0.2",
        "pg8000>=1.12.3,<1.13.0",
    ],
)
