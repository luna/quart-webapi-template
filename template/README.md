# PROJECT_NAME

PROJECT_DESCRIPTION

## setup

### postgresql

```
postgres=# create user PROJECT_NAME with password '123';
CREATE ROLE
postgres=# create database PROJECT_NAME;
CREATE DATABASE
postgres=# grant all privileges on database PROJECT_NAME to PROJECT_NAME;
GRANT
```

### python

```
git clone ...
cd ...

# you can also use a virtual environment (recommended)
python3 -m pip install --editable .

# specifics of creating the database and user for it are deploy-specific.
cp config.example.toml config.toml

# arguments may be different to load the schema
psql -U PROJECT_NAME -f schema.sql

# prepare database for future migrations
./agnwrapper.py bootstrap

hypercorn PROJECT_NAME --access-log - --bind 0.0.0.0:6900
```

### Creating a database migration

**You will both need to create the migration and update `schema.sql` to reflect
your changes.**

The need to keep the `schema.sql` file instead of making it a
migration itself is for ease of development. It is easier for any developer to
peek at the `schema.sql` file and have a full view of the database, instead of
having to go through N migrations to find out the final state of a table.

```
./agnwrapper.py create_migration name_for_migration_goes_here
```
