#!/usr/bin/env python3

import pprint
import subprocess
import shutil
import sys
import datetime

from pathlib import Path


def copy2_with_output(src, dst):
    print(f"{src} -> {dst}")
    shutil.copy2(src, dst)


def main():
    self_path = Path(sys.argv[0])
    template_path = self_path.parent / "template"

    project = {}
    cur = 1
    for attr in ("name", "description", "license", "author", "url", "output"):
        try:
            project[attr] = sys.argv[cur]
        except IndexError:
            print("usage: {self_path} name description license author url output_path")
            sys.exit(1)
        cur += 1

    output_path = Path(project["output"])

    pprint.pprint(project)
    shutil.copytree(template_path, output_path, copy_function=copy2_with_output)

    name = project["name"]
    author = project["author"]
    year = datetime.datetime.utcnow().year
    copyright_notice = (
        f"# {name}: {project['description']}\n"
        f"# Copyright {year}, {author} and {name} contributors\n"
        f"# SPDX-License-Identifier: {project['license']}\n"
    )

    for filepath in output_path.glob("**/*"):
        if filepath.is_dir():
            continue
        file_contents = filepath.open(mode="r").read()
        file_contents = file_contents.replace("# COPYRIGHT_HEADER", copyright_notice)

        for attr, value in project.items():
            attr_marker = f"PROJECT_{attr}".upper()
            file_contents = file_contents.replace(attr_marker, value)

        filepath.open(mode="w").write(file_contents)
        print(filepath, "ok")

    root_package = output_path / "project_root_package"
    new_root_package = output_path / name
    print("rename", root_package, new_root_package)
    root_package.rename(new_root_package)


if __name__ == "__main__":
    main()
